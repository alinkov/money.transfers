## About
This is small application for transfer money from one account to another


This application works with local storage in map, but you can override **ru.test.money.transfer.repository.AccountRepository** for work with real database. Also you can add work with transactions.

In this application used next libraries:

 +  Sparkjava
 +  Gradle
 +  Jackson
 +  Apache commons
 +  Google guice
 +  Junit

## Start

For start this application you need to build project and run **ru.test.money.transfer.MoneyTransfer.main**


You can set the port, if you add the argument in method

## Test

You can run the tests **ru.test.money.transfer.controller.TransferControllerTest**


