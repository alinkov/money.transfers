package ru.test.money.transfer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import ru.test.money.transfer.controller.TransferController;

import static spark.Spark.port;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Main class to start Application
 */
public class MoneyTransfer {

    public static void main(String[] args) {
        // Start embedded server at this port
        final int port = (args != null && args.length == 1) ? Integer.valueOf(args[0]) : 8081;
        port(port);
        start();
    }

    public static void start() {
        final Injector injector = Guice.createInjector(new TransferModule());
        final TransferController transferController = injector.getInstance(TransferController.class);
        transferController.establishRoutes();
    }
}
