package ru.test.money.transfer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.test.money.transfer.model.Account;
import ru.test.money.transfer.model.MessageResponse;
import ru.test.money.transfer.model.TransferInfo;
import ru.test.money.transfer.service.AccountService;
import spark.Route;

import javax.inject.Inject;
import java.util.Optional;

import static java.net.HttpURLConnection.*;
import static spark.Spark.*;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Base controller for work with transfers
 */
public class TransferController {
    private static final String ACCOUNT_NOT_FOUND = "account not found";
    @Inject
    private AccountService accountService;
    private static ObjectMapper om = new ObjectMapper();

    /**
     * Create routes for all path for account
     */
    public void establishRoutes() {
        // Fetch all accounts
        get("/", getRoute((request, response) -> accountService.fetchAll()));

        // POST - Add an account
        post("/", getRoute((request, response) -> {
            final String body = new String(request.bodyAsBytes());
            final Account account = om.readValue(body, Account.class);
            final Account result = accountService.create(account);
            response.status(HTTP_CREATED);
            return result;
        }));

        // GET - Fetch account by id
        get("/:id", getRoute((request, response) -> {
            final Optional<Account> optionalAccount = accountService.findById(Integer.valueOf(request.params(":id")));
            if (optionalAccount.isPresent()) {
                return optionalAccount.get();
            } else {
                response.status(HTTP_NOT_FOUND);
                return MessageResponse.error(HTTP_NOT_FOUND, ACCOUNT_NOT_FOUND);
            }
        }));

        // PUT - Update account
        put("/:id", getRoute((request, response) -> {
            final String id = request.params(":id");
            final Optional<Account> optionalAccount = accountService.findById(Integer.valueOf(request.params(":id")));
            if (optionalAccount.isPresent()) {
                final String body = new String(request.bodyAsBytes());
                final Account account = om.readValue(body, Account.class);
                accountService.update(account);
                return MessageResponse.ok("Account with id " + id + " is updated!");
            } else {
                response.status(HTTP_NOT_FOUND);
                return MessageResponse.error(HTTP_NOT_FOUND, ACCOUNT_NOT_FOUND);
            }
        }));

        // DELETE - delete account
        delete("/:id", getRoute((request, response) -> {
            final Optional<Account> optionalAccount = accountService.findById(Integer.valueOf(request.params(":id")));
            if (optionalAccount.isPresent()) {
                final Account account = optionalAccount.get();
                final boolean deleteResult = accountService.delete(account);
                if (deleteResult) {
                    return MessageResponse.ok("Account with id " + account.getId() + " is deleted!");
                } else {
                    return MessageResponse.error("Account with id " + account.getId() + " is not deleted.");
                }
            } else {
                response.status(HTTP_NOT_FOUND);
                return MessageResponse.error(HTTP_NOT_FOUND, ACCOUNT_NOT_FOUND);
            }
        }));

        // Transfer money from one account to another
        post("/transfer/", getRoute((request, response) -> {
            final String body = new String(request.bodyAsBytes());
            final TransferInfo transferInfo = om.readValue(body, TransferInfo.class);

            final boolean transferResult = accountService.transfer(transferInfo);
            if (transferResult) {
                response.status(HTTP_OK);
                return MessageResponse.ok("Transfer success");
            } else {
                response.status(HTTP_BAD_REQUEST);
                return MessageResponse.error("Transfer with error");
            }
        }));
    }

    /**
     * Create Route with Handler of Exceptions
     */
    private static Route getRoute(Route route) {
        return (request, response) -> {
            try {
                final Object result = route.handle(request, response);
                return om.writeValueAsString(result);
            } catch (Exception e) {
                response.status(HTTP_BAD_REQUEST);
                return om.writeValueAsString(new MessageResponse(HTTP_BAD_REQUEST, e.getMessage()));
            }
        };
    }
}
