package ru.test.money.transfer.model;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Random;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * This is account model
 */
public class Account implements Serializable {
    /**Id of account*/
    private final int id;
    /**Account money*/
    private final BigDecimal money;

    public Account() {
        this(new Random().nextInt(), BigDecimal.ZERO);
    }

    public Account(int id, BigDecimal money) {
        this.id = id;
        this.money = money;
    }

    public Account(Account account, BigDecimal money) {
        this.id = account.getId();
        this.money = money;
    }

    public int getId() {
        return id;
    }

    public BigDecimal getMoney() {
        return money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(money, account.money);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, money);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", money=" + money +
                '}';
    }
}
