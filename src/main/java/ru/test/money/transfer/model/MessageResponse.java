package ru.test.money.transfer.model;

import java.io.Serializable;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Model for response
 */
public class MessageResponse implements Serializable {
    /**Message of the response*/
    private String message;
    /**Code of the response*/
    private int code;


    public MessageResponse() {
    }

    public MessageResponse(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public static MessageResponse error(String message) {
        return error(HTTP_BAD_REQUEST, message);
    }

    public static MessageResponse error(int code, String message) {
        return new MessageResponse(code, message);
    }

    public static MessageResponse ok(String message) {
        return new MessageResponse(HTTP_OK, message);
    }
}
