package ru.test.money.transfer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Information for transfer from one account to another
 */
public class TransferInfo implements Serializable {
    /**id of the account, which the money will be debited */
    private final int from;
    /**id of the account for receiving money */
    private final int to;
    /**amount of money*/
    private final BigDecimal amount;

    public TransferInfo() {
        this(0, 0, BigDecimal.ZERO);
    }

    public TransferInfo(int from, int to, BigDecimal amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransferInfo that = (TransferInfo) o;
        return from == that.from &&
                to == that.to &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, amount);
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "from=" + from +
                ", to=" + to +
                ", amount=" + amount +
                '}';
    }
}
