package ru.test.money.transfer.repository;

import com.google.inject.ImplementedBy;
import ru.test.money.transfer.model.Account;

import java.util.List;
import java.util.Optional;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Repository interface for Account
 */
@ImplementedBy(AccountRepositoryImpl.class)
public interface AccountRepository {
    /**
     * Create account
     * @param account - account to create
     * @return  saved account
     */
    Account create(Account account);

    /**
     * Find account by id
     * @param id    id of the account
     * @return  account
     */
    Optional<Account> findById(int id);

    /**
     * Fetch all accounts
     * @return  all accounts
     */
    List<Account> fetchAll();

    /**
     * Update account
     * @param account   account to update
     */
    void update(Account account);

    /**
     * Delete account
     * @param account   account to delete
     * @return result of delete
     */
    boolean delete(Account account);
}
