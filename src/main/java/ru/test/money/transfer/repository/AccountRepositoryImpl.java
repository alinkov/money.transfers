package ru.test.money.transfer.repository;

import org.apache.commons.lang3.Validate;
import ru.test.money.transfer.model.Account;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 */
public class AccountRepositoryImpl implements AccountRepository {
    private static final Map<Integer, Account> ACCOUNTS = Collections.synchronizedMap(new LinkedHashMap<>());

    /**{@inheritDoc}*/
    @Override
    public Account create(Account account) {
        checkForNull(account);
        if (ACCOUNTS.containsKey(account.getId())) {
            throw new IllegalArgumentException("Account with id = " + account.getId() + " already exists");
        }
        ACCOUNTS.put(account.getId(), account);
        return account;
    }

    /**{@inheritDoc}*/
    @Override
    public Optional<Account> findById(int id) {
        if (ACCOUNTS.containsKey(id)) {
            return Optional.of(ACCOUNTS.get(id));
        } else {
            return Optional.empty();
        }
    }

    /**{@inheritDoc}*/
    @Override
    public List<Account> fetchAll() {
        return Collections.unmodifiableList(new ArrayList<>(ACCOUNTS.values()));
    }

    /**{@inheritDoc}*/
    @Override
    public void update(Account account) {
        checkForNull(account);
        if (!ACCOUNTS.containsKey(account.getId())) {
            throw new IllegalArgumentException("Can not update account, because account has not been saved");
        }
        ACCOUNTS.put(account.getId(), account);
    }

    /**{@inheritDoc}*/
    @Override
    public boolean delete(Account account) {
        checkForNull(account);
        return ACCOUNTS.remove(account.getId()) != null;
    }

    private void checkForNull(Account account) {
        Validate.notNull(account, "Account can not be null");
    }
}
