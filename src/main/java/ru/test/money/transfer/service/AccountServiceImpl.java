package ru.test.money.transfer.service;

import org.apache.commons.lang3.Validate;
import ru.test.money.transfer.model.Account;
import ru.test.money.transfer.model.TransferInfo;
import ru.test.money.transfer.repository.AccountRepository;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 *
 * Implementation of {@link AccountService}
 */
public class AccountServiceImpl implements AccountService {
    private static final int DEFAULT_TIMEOUT = 5;
    private static Map<Integer, ReentrantLock> locks = new ConcurrentHashMap<>();

    @Inject
    private AccountRepository accountRepository;

    /**{@inheritDoc}*/
    @Override
    public boolean transfer(TransferInfo transferInfo) {
        Validate.notNull(transferInfo);
        try {
            createLocks(transferInfo.getFrom(), transferInfo.getTo());
            final Account accountFrom = findById(transferInfo.getFrom()).orElseThrow(() -> new IllegalArgumentException("Can not find from account"));
            final Account accountTo = findById(transferInfo.getTo()).orElseThrow(() -> new IllegalArgumentException("Can not find to account"));
            if (accountFrom.getMoney().compareTo(transferInfo.getAmount()) >= 0 && transferInfo.getAmount().compareTo(BigDecimal.ZERO) >= 0) {
                accountRepository.update(new Account(accountFrom, accountFrom.getMoney().subtract(transferInfo.getAmount())));
                accountRepository.update(new Account(accountTo, accountTo.getMoney().add(transferInfo.getAmount())));
                return true;
            } else {
                return false;
            }
        } finally {
            unlock(transferInfo.getFrom(), transferInfo.getTo());
        }
    }

    /**{@inheritDoc}*/
    @Override
    public Account create(Account account) {
        Validate.notNull(account);
        Validate.isTrue(account.getMoney() != null && account.getMoney().compareTo(BigDecimal.ZERO) >= 0, "Incorrect amount of money");
        try {
            createLock(account.getId());
            return accountRepository.create(account);
        } finally {
            unlock(account.getId());
        }
    }

    /**{@inheritDoc}*/
    @Override
    public Optional<Account> findById(int id) {
        return accountRepository.findById(id);
    }

    /**{@inheritDoc}*/
    @Override
    public List<Account> fetchAll() {
        return accountRepository.fetchAll();
    }

    /**{@inheritDoc}*/
    @Override
    public void update(Account account) {
        Validate.notNull(account);
        Validate.isTrue(account.getMoney() != null && account.getMoney().compareTo(BigDecimal.ZERO) >= 0, "Incorrect amount of money");
        try {
            createLock(account.getId());
            accountRepository.update(account);
        } finally {
            unlock(account.getId());
        }
    }

    /**{@inheritDoc}*/
    @Override
    public boolean delete(Account account) {
        Validate.notNull(account);
        try {
            createLock(account.getId());
            return accountRepository.delete(account);
        } finally {
            unlock(account.getId());
        }
    }

    /**
     * Create lock
     * @param lockId id of the lock
     */
    private static synchronized void createLock(int lockId) {
        if (locks.containsKey(lockId)) {
            final boolean lockResult;
            try {
                final ReentrantLock lock = locks.get(lockId);
                lockResult = lock.tryLock() || lock.tryLock(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            Validate.isTrue(lockResult, "Lock time out");
        } else {
            final ReentrantLock lock = new ReentrantLock();
            locks.put(lockId, lock);
            lock.lock();
        }
    }

    /**
     * Create two lock
     * @param lockId1   id of the lock1
     * @param lockId2   id of the lock2
     */
    private static synchronized void createLocks(int lockId1, int lockId2) {
        createLock(lockId1);
        createLock(lockId2);
    }

    /**
     * Unlock
     * @param lockId id of the lock
     */
    private static void unlock(int lockId) {
        final ReentrantLock lock = locks.get(lockId);
        if (lock != null && lock.isHeldByCurrentThread()) {
            lock.unlock();
        }
    }

    /**
     * Unlock two locks
     * @param lockId1   id of the lock1
     * @param lockId2   id of the lock2
     */
    private static void unlock(int lockId1, int lockId2) {
        unlock(lockId1);
        unlock(lockId2);
    }
}
