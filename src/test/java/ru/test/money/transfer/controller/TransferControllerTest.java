package ru.test.money.transfer.controller;

import com.despegar.http.client.DeleteMethod;
import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpMethod;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.http.client.PutMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.ClassRule;
import org.junit.Test;
import ru.test.money.transfer.MoneyTransfer;
import ru.test.money.transfer.model.Account;
import ru.test.money.transfer.model.MessageResponse;
import ru.test.money.transfer.model.TransferInfo;
import spark.Spark;
import spark.servlet.SparkApplication;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static java.net.HttpURLConnection.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by ALinkov<br/>
 * Date: 06.05.2018<br/>
 */
public class TransferControllerTest {

    private static final String BASE_URL = "/";
    private static final String TRANSFER_URL = "/transfer/";
    private static final Random RANDOM = new Random();
    private static Integer randomPort = 1000 + RANDOM.nextInt(60000);
    private static ObjectMapper om = new ObjectMapper();

    public static class BoardBoxControllerTestSparkApplication implements SparkApplication {

        @Override
        public void init() {
            MoneyTransfer.start();
        }

        @Override
        public void destroy() {
            Spark.stop();
        }
    }

    @ClassRule
    public static final SparkServer<BoardBoxControllerTestSparkApplication> TEST_SERVER = new SparkServer<>(BoardBoxControllerTestSparkApplication.class, randomPort);

    @Test
    public void testFetchAll() throws Exception {
        assertEquals(HTTP_OK, get(BASE_URL, ArrayList.class).getCode());
    }

    @Test
    public void testSave() throws Exception {
        final Account savedAccount = saveAccount();
        assertEquals(savedAccount, get(BASE_URL + savedAccount.getId(), Account.class).getResult());
    }

    @Test
    public void testUpdate() throws Exception {
        final Account savedAccount = saveAccount();
        assertEquals(savedAccount, get(BASE_URL + savedAccount.getId(), Account.class).getResult());
        final Account updatedAccount = new Account(savedAccount, savedAccount.getMoney().add(BigDecimal.ONE));
        put(BASE_URL + updatedAccount.getId(), updatedAccount, MessageResponse.class);
        assertEquals(updatedAccount, get(BASE_URL + updatedAccount.getId(), Account.class).getResult());
    }

    @Test
    public void testCorrectTransfer() throws Exception {
        final Account account1 = saveAccount(100);
        final Account account2 = saveAccount(200);
        final BigDecimal amount = BigDecimal.valueOf(50);
        final Answer<MessageResponse> transferAnswer = post(TRANSFER_URL, new TransferInfo(account1.getId(), account2.getId(), amount), MessageResponse.class);
        assertEquals(HTTP_OK, transferAnswer.getCode());
        assertEquals(account1.getMoney().subtract(amount), getById(account1.getId()).getMoney());
        assertEquals(account2.getMoney().add(amount), getById(account2.getId()).getMoney());
    }

    @Test
    public void testMultithreadTransfer() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        final Account account1 = saveAccount(1000);
        final Account account2 = saveAccount(1000);
        final Random random = new Random();
        final BigDecimal amount = BigDecimal.valueOf(5);
        final List<Callable<Boolean>> tasks = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            tasks.add(() -> {
                final Answer<MessageResponse> transferAnswer;
                try {
                    if (random.nextBoolean()) {
                        transferAnswer = post(TRANSFER_URL, new TransferInfo(account1.getId(), account2.getId(), amount), MessageResponse.class);
                    } else {
                        transferAnswer = post(TRANSFER_URL, new TransferInfo(account2.getId(), account1.getId(), amount), MessageResponse.class);
                    }
                    return HTTP_OK == transferAnswer.getCode();
                } catch (Exception e) {
                    return false;
                }
            });
        }
        final List<Future<Boolean>> results = executorService.invokeAll(tasks);
        for (Future<Boolean> result : results) {
            assertTrue(result.get());
        }
    }

    @Test
    public void testIncorrectTransfer() throws Exception {
        final Account account1 = saveAccount(100);
        final Account account2 = saveAccount(200);
        final BigDecimal amount = BigDecimal.valueOf(200);
        final Answer<MessageResponse> transferAnswer = post(TRANSFER_URL, new TransferInfo(account1.getId(), account2.getId(), amount), MessageResponse.class);
        assertEquals(HTTP_BAD_REQUEST, transferAnswer.getCode());
        assertEquals(account1.getMoney(), getById(account1.getId()).getMoney());
        assertEquals(account2.getMoney(), getById(account2.getId()).getMoney());
    }

    @Test
    public void testTransferToIncorrectAccount() throws Exception {
        final Account account1 = saveAccount(100);
        final BigDecimal amount = BigDecimal.valueOf(50);
        final int incorrectAccountId = -1;
        final Answer<MessageResponse> transferAnswer = post(TRANSFER_URL, new TransferInfo(account1.getId(), incorrectAccountId, amount), MessageResponse.class);
        assertEquals(HTTP_BAD_REQUEST, transferAnswer.getCode());
        assertEquals(account1.getMoney(), getById(account1.getId()).getMoney());
    }

    @Test
    public void testDelete() throws Exception {
        final Account account = saveAccount();
        final Answer<MessageResponse> deleteAnswer = delete(BASE_URL + account.getId(), MessageResponse.class);
        assertEquals(HTTP_OK, deleteAnswer.getCode());
        final Answer<MessageResponse> accountAnswer = get(BASE_URL + account.getId(), MessageResponse.class);
        assertEquals(HTTP_NOT_FOUND, accountAnswer.getCode());
    }

    @Test
    public void testIncorrectDelete() throws Exception {
        final Answer<MessageResponse> deleteAnswer = delete(BASE_URL + "-1", MessageResponse.class);
        assertEquals(HTTP_NOT_FOUND, deleteAnswer.getCode());
    }

    private <T> Answer<T> post(String path, Object payload, Class<T> resultClass) throws Exception {
        final PostMethod resp = TEST_SERVER.post(path, om.writeValueAsString(payload), false);
        return getAnswer(resp, resultClass);
    }

    private <T> Answer<T> get(String url, Class<T> resultClass) throws Exception {
        final GetMethod resp = TEST_SERVER.get(url, false);
        return getAnswer(resp, resultClass);
    }

    private <T> Answer<T> put(String path, Object payload, Class<T> resultClass) throws Exception {
        final PutMethod resp = TEST_SERVER.put(path, om.writeValueAsString(payload), false);
        return getAnswer(resp, resultClass);
    }

    private <T> Answer<T> delete(String path, Class<T> resultClass) throws Exception {
        final DeleteMethod resp = TEST_SERVER.delete(path, false);
        return getAnswer(resp, resultClass);
    }

    private <T> Answer<T> getAnswer(HttpMethod httpMethod, Class<T> resultClass) throws Exception {
        final HttpResponse execute = TEST_SERVER.execute(httpMethod);
        return new Answer<>(om.readValue(new String(execute.body()), resultClass), execute.code());
    }

    private Account getById(int id) throws Exception {
        return get(BASE_URL + id, Account.class).getResult();
    }

    private Account saveAccount() throws Exception {
        return saveAccount(RANDOM.nextInt(100000));
    }

    private Account saveAccount(int money) throws Exception {
        final Account account = new Account(RANDOM.nextInt(), BigDecimal.valueOf(money));
        final Answer<Account> savedAccount = post(BASE_URL, account, Account.class);
        assertEquals(account, savedAccount.getResult());
        assertEquals(HTTP_CREATED, savedAccount.getCode());
        return savedAccount.getResult();
    }

    private static class Answer<T> {
        private final T result;
        private final int code;

        public Answer(T result, int code) {
            this.result = result;
            this.code = code;
        }

        public T getResult() {
            return result;
        }

        public int getCode() {
            return code;
        }
    }
}